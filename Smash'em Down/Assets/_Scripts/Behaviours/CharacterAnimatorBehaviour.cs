﻿using MightyAttributes;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterAnimatorBehaviour : MonoBehaviour
{    
    public enum AnimatorState : byte
    {
        Idle,
        Run,
        Jump,
        Tumble
    }
    
    // @formatter:off

    [SerializeField, GetComponent, ReadOnly] private Animator _animator;
    [SerializeField, AnimatorParameter("Reset"), ReadOnly] private int _resetID;
    [SerializeField, AnimatorParameter("Run"), ReadOnly] private int _runID;
    [SerializeField, AnimatorParameter("Jump"), ReadOnly] private int _jumpID;
    [SerializeField, AnimatorParameter("Tumble"), ReadOnly] private int _tumbleID;
    
    [ShowNonSerialized] private AnimatorState m_animatorState;
    
    // @formatter:on

    public void SetAnimatorSpeed(float speed) => _animator.speed = speed;

    public void SetAnimatorState(AnimatorState state)
    {
        if (m_animatorState == state) return;

        m_animatorState = state;
        switch (state)
        {
            case AnimatorState.Idle:
                _animator.ResetTrigger(_runID);
                _animator.ResetTrigger(_jumpID);
                _animator.ResetTrigger(_tumbleID);
                
                _animator.SetTrigger(_resetID);
                break;
            case AnimatorState.Run:
                _animator.ResetTrigger(_resetID);
                _animator.ResetTrigger(_jumpID);
                _animator.ResetTrigger(_tumbleID);
                
                _animator.SetTrigger(_runID);
                break;
            case AnimatorState.Jump:
                _animator.ResetTrigger(_resetID);
                _animator.ResetTrigger(_runID);
                _animator.ResetTrigger(_tumbleID);
                
                _animator.SetTrigger(_jumpID);
                break;
            case AnimatorState.Tumble:
                _animator.ResetTrigger(_resetID);
                _animator.ResetTrigger(_runID);
                _animator.ResetTrigger(_jumpID);
                
                _animator.SetTrigger(_tumbleID);
                break;
        }
    }
}

﻿using MightyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    [SerializeField] private GameObject _startPanel;
    [SerializeField] private Button _nextButton, _restartButton;
    [SerializeField, ButtonArray] private Image[] _heartImages;

    public void DisplayStartPanel(bool display) => _startPanel.SetActive(display);
    public void DisplayNextButton(bool display) => _nextButton.gameObject.SetActive(display);
    public void DisplayRestartButton(bool display) => _restartButton.gameObject.SetActive(display);

    public void Init()
    {
        DisplayStartPanel(true);
        DisplayNextButton(false);
        DisplayRestartButton(false);
    }

    public void SetHP(int hp)
    {
        for (var i = 0; i < _heartImages.Length; i++)
            _heartImages[i].color = i < hp ? Color.white : Color.black;
    }
}
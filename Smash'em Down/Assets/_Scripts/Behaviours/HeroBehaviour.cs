﻿using MightyAttributes;
using UnityEngine;

public class HeroBehaviour : BaseCharacterBehaviour
{
    private enum HeroState : byte
    {
        Idle,
        Shielding,
        Sprinting,
        Jumping
    }

    private const int MAX_HP = 3;

    private static float m_fixedDeltaTime;

    // @formatter:off
    
    [Box("Movements"), SerializeField] private Vector3 _startPositionOffset;
    [Box("Movements"), SerializeField] private float _jumpSpeed;
    [Box("Movements"), SerializeField] private AnimationCurve _jumpCurve;
    [Box("Movements"), SerializeField] private AnimationCurve _smashSlowMotionCurve;
    [Box("Movements"), SerializeField] private AnimationCurve _invincibleSlowMotionCurve;
    
    [Box("Collisions"), SerializeField] private GameObject _shield; 
    [Box("Collisions"), SerializeField] private float _invincibleDuration; 
    
    [Box("Animations"), SerializeField, GetComponentInChildren, ReadOnly] private CharacterAnimatorBehaviour _characterAnimator;
    [Box("Animations"), SerializeField] private float _animatorNormalSpeed;
    [Box("Animations"), SerializeField] private float _animatorSprintSpeed;
    
    [Fold("Non Serialized"), ShowNonSerialized] private HeroState m_heroState;

    // @formatter:on

    private float m_previousMouseX;
    private float m_mouseDelta;

    private int m_currentHP;
    private bool m_invincible;
    private float m_invincibleTimer;

    private float m_jumpDuration;
    private float m_jumpTimer;
    private BaseCriminalBehaviour m_targetedCriminal;

    public bool IsShielding => m_heroState == HeroState.Shielding;
    
    public Vector3 StartPositionOffset => _startPositionOffset;

    public void Init(Vector3 position, Quaternion rotation)
    {
        if (!m_init) m_fixedDeltaTime = Time.fixedDeltaTime;

        base.Init();

        SetHP(MAX_HP);
        m_transform.position = position + _startPositionOffset;
        m_transform.rotation = rotation;

        Stop();
    }

    public override void Play()
    {
        base.Play();
        
        SetHP(MAX_HP);
        SetHeroState(Input.GetMouseButton(0) ? HeroState.Sprinting : HeroState.Shielding);
    }

    public override void Stop()
    {
        base.Stop();

        SetHeroState(HeroState.Idle);
    }

    public void JumpTowards(BaseCriminalBehaviour criminal)
    {
        if (!m_init || !m_playing) return;

        if (m_heroState != HeroState.Sprinting) return;

        m_targetedCriminal = criminal;
        SetHeroState(HeroState.Jumping);
    }

    public void LoseLife()
    {
        if (!m_init || !m_playing) return;

        if (m_invincible || m_heroState != HeroState.Sprinting) return;

        if (!SetHP(m_currentHP - 1)) return;

        m_invincible = true;
        m_invincibleTimer = 0;

        _characterAnimator.SetAnimatorSpeed(_animatorNormalSpeed);
        _characterAnimator.SetAnimatorState(CharacterAnimatorBehaviour.AnimatorState.Tumble);
    }

    private bool SetHP(int hp)
    {
        m_currentHP = hp;
        GameManager.Instance.GUIManager.SetHP(hp);

        if (hp >= 0) return true;

        Stop();
        GameManager.Instance.LevelManager.LoseLevel();

        return false;
    }

    private void SetHeroState(HeroState state)
    {
        if (m_heroState == state) return;

        m_heroState = state;
        switch (state)
        {
            case HeroState.Idle:
                _characterAnimator.SetAnimatorSpeed(_animatorNormalSpeed);
                _characterAnimator.SetAnimatorState(CharacterAnimatorBehaviour.AnimatorState.Idle);
                ChangeTimeScale(1);
                break;
            case HeroState.Shielding:
                _shield.SetActive(true);
                _characterAnimator.SetAnimatorSpeed(_animatorNormalSpeed);
                _characterAnimator.SetAnimatorState(CharacterAnimatorBehaviour.AnimatorState.Run);
                m_mouseDelta = 0;
                ChangeTimeScale(1);
                break;
            case HeroState.Sprinting:
                _shield.SetActive(false);
                _characterAnimator.SetAnimatorSpeed(_animatorSprintSpeed);
                _characterAnimator.SetAnimatorState(CharacterAnimatorBehaviour.AnimatorState.Run);
                m_previousMouseX = Input.mousePosition.x;
                ChangeTimeScale(1);
                break;
            case HeroState.Jumping:
                _characterAnimator.SetAnimatorSpeed(_animatorNormalSpeed);
                _characterAnimator.SetAnimatorState(CharacterAnimatorBehaviour.AnimatorState.Jump);
                m_jumpDuration = (m_targetedCriminal.GetPosition() - GetPosition()).magnitude / _jumpSpeed;
                m_jumpTimer = 0;
                break;
        }
    }

    public void UpdateHero()
    {
        if (!m_init || !m_playing) return;

        if (m_heroState == HeroState.Idle || m_heroState == HeroState.Jumping) return;

        SetHeroState(Input.GetMouseButton(0) ? HeroState.Sprinting : HeroState.Shielding);

        if (m_heroState != HeroState.Sprinting) return;

        var mouseX = Input.mousePosition.x;
        m_mouseDelta = mouseX - m_previousMouseX;
        m_previousMouseX = mouseX;
    }

    public void FixedUpdateHero()
    {
        if (!m_init || !m_playing) return;

        if (m_heroState == HeroState.Idle) return;

        if (m_invincible)
        {
            ChangeTimeScale(_invincibleSlowMotionCurve.Evaluate(m_invincibleTimer / _invincibleDuration));

            m_invincibleTimer += Time.deltaTime;
            if (m_invincibleTimer >= _invincibleDuration)
            {
                m_invincible = false;
                _characterAnimator.SetAnimatorSpeed(m_heroState == HeroState.Sprinting ? _animatorSprintSpeed : _animatorNormalSpeed);
                _characterAnimator.SetAnimatorState(CharacterAnimatorBehaviour.AnimatorState.Run);
            }
        }

        ApplyRotation();
        
        switch (m_heroState)
        {
            case HeroState.Shielding:
                ApplyMovement(GetForwardsMovement(false));
                break;
            case HeroState.Sprinting:
                ApplyMovement(GetForwardsMovement(!m_invincible) + GetSidewayMovement(m_mouseDelta));
                break;
            case HeroState.Jumping:
                var deltaTime = Time.deltaTime;
                var timerProgression = m_jumpTimer / m_jumpDuration;

                var targetPosition = m_targetedCriminal.GetPosition();
                var destination = new Vector3(targetPosition.x, targetPosition.y + _jumpCurve.Evaluate(timerProgression), targetPosition.z);

                _rigidbody.MovePosition(Vector3.MoveTowards(GetPosition(), destination, _jumpSpeed * deltaTime));

                ChangeTimeScale(_smashSlowMotionCurve.Evaluate(timerProgression));

                m_jumpTimer += deltaTime;
                if (m_jumpTimer >= m_jumpDuration)
                {
                    m_targetedCriminal.Hit();
                    
                    if (m_heroState != HeroState.Idle)
                        SetHeroState(Input.GetMouseButton(0) ? HeroState.Sprinting : HeroState.Shielding);
                }

                break;
        }
    }

    public static void ChangeTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
        Time.fixedDeltaTime = m_fixedDeltaTime * timeScale;
    }
}
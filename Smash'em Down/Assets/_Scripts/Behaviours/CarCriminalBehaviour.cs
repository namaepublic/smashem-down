﻿using MightyAttributes;
using UnityEngine;

public class CarCriminalBehaviour : BaseCriminalBehaviour
{
    [Box("Hero Detection"), SerializeField] private float _explosionForce;

    protected override void OnHeroTriggered() => GameManager.Instance.HeroBehaviour.JumpTowards(this);

    public override void Hit()
    {
        Smash();
        
        _rigidbody.AddForce(_explosionForce * new Vector3(Random.Range(-1, 1f), Random.value, Random.value), ForceMode.Impulse);
        _rigidbody.AddTorque(_explosionForce * Random.insideUnitSphere, ForceMode.Impulse);
    }
}
﻿using System;
using MightyAttributes;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class TrackSection
{
    // @formatter:off
    
    [Box] [SerializeField, AssetOnly, ButtonArray] private VehicleBehaviour[] _vehiclePrefabs;
    [Box] [SerializeField, AssetOnly] private TrackPatternBehaviour _trackPatternPrefab;
    
    [Box] [SerializeField, MinMaxSlider("MinValue", "MaxValue")] private Vector2Int _vehicleAmountPerBlock;
    
    // @formatter:on

    public TrackPatternBehaviour TrackPatternPrefab => _trackPatternPrefab;
    public VehicleBehaviour[] VehiclePrefabs => _vehiclePrefabs;

    public int GetAmountOfVehicles() => Random.Range(_vehicleAmountPerBlock.x, _vehicleAmountPerBlock.y);

#if UNITY_EDITOR
    private float MinValue => 0;
    private float MaxValue => _trackPatternPrefab.GetMaxAmountOfVehicles();
#endif
}

public class LevelManager : MonoBehaviour
{
    // @formatter:off
    
    [SerializeField, PositionHandle] private Vector3 _startPosition;
    [SerializeField] private Vector3 _startRotation;

    [SerializeField] private Transform _trackTransform;
    [SerializeField] private Transform _criminalsTransform;
    
    [SerializeField, AssetOnly, ButtonArray] private BaseCriminalBehaviour[] _criminalPrefabs;
    
    [FoldArea("Track Sections")]
    [SerializeField, ButtonArray(ArrayOption.ContentOnly), Nest(NestOption.ContentOnly)] private TrackSection[] _trackSections; 
    
    // @formatter:on

    private int m_criminalIndex;
    private BaseCriminalBehaviour CurrentCriminal { get; set; }

    private TrackPatternBehaviour PreviousPattern { get; set; }
    private TrackPatternBehaviour CurrentPattern { get; set; }
    private TrackPatternBehaviour NextPattern { get; set; }

    public Transform CriminalsTransform => _criminalsTransform;

    private void Start() => Init();

    public void Init()
    {
        GameManager.Instance.WaitForPlay(this);
        GameManager.Instance.HeroBehaviour.Init(_startPosition, Quaternion.Euler(_startRotation));

        var section = _trackSections[Random.Range(0, _trackSections.Length)];
        var prefab = section.TrackPatternPrefab;

        NextPattern = Instantiate(prefab, _startPosition, Quaternion.Euler(_startRotation), _trackTransform);
        NextPattern.SpawnVehicles(section);
        InitPattern(NextPattern);

        m_criminalIndex = -1;

        SpawnNextCriminal(_startPosition + GameManager.Instance.HeroBehaviour.StartPositionOffset);
    }

    public void PlayLevel()
    {
        PlayCurrentCriminal();
        PlayPattern(CurrentPattern);
    }

    public void LoseLevel()
    {
        GameManager.Instance.WaitForRestart();
    }

    public void WinLevel()
    {
        GameManager.Instance.WaitForNext();
    }

    public void FixedUpdateManager()
    {
        CurrentCriminal.FixedUpdateCriminal();
        foreach (var vehicle in CurrentPattern.VehicleBehaviours)
            vehicle.FixedUpdateVehicle();
    }

    public bool SpawnNextCriminal(Vector3 heroPosition)
    {
        m_criminalIndex++;
        if (m_criminalIndex >= _criminalPrefabs.Length)
        {
            WinLevel();
            return false;
        }

        if (CurrentPattern &&
            CurrentPattern.SpawnCriminal(heroPosition, _criminalPrefabs[m_criminalIndex]) is BaseCriminalBehaviour criminal)
            CurrentCriminal = criminal;
        else
            CurrentCriminal = NextPattern.SpawnCriminal(heroPosition, _criminalPrefabs[m_criminalIndex]);

        CurrentCriminal.Init();
        return true;
    }

    public void LoadNextPattern(TrackPatternBehaviour triggeredPattern)
    {
        UnloadPreviousPattern();

        PreviousPattern = CurrentPattern;
        CurrentPattern = triggeredPattern;

        var section = _trackSections[Random.Range(0, _trackSections.Length - 1)];
        var prefab = section.TrackPatternPrefab;

        NextPattern = Instantiate(prefab, CurrentPattern.GetEndPosition(), Quaternion.Euler(CurrentPattern.EndRotation), _trackTransform);
        NextPattern.SpawnVehicles(section);
        InitPattern(NextPattern);
    }

    public void PlayCurrentCriminal() => CurrentCriminal.Play();

    public void InitPattern(TrackPatternBehaviour pattern)
    {
        foreach (var vehicle in pattern.VehicleBehaviours)
            vehicle.Init();
    }

    public void PlayPattern(TrackPatternBehaviour pattern)
    {
        foreach (var vehicle in pattern.VehicleBehaviours)
            vehicle.Play();
    }

    public void UnloadPreviousPattern()
    {
        if (PreviousPattern) Destroy(PreviousPattern.gameObject);
    }
}
﻿using MightyAttributes;
using MightyAttributes.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

public abstract class BaseCriminalBehaviour : BaseCharacterBehaviour
{
    protected enum CriminalState : byte
    {
        Idle,
        Moving,
        ChangingDirection
    }

    // @formatter:off
    
    [Box("Direction"), SerializeField] private float _directionChangeCooldown;
    [Box("Direction"), SerializeField, PercentSlider(true)] private float _directionChangeRate;
    [Box("Direction"), SerializeField, MinMax] private Vector2 _directionChangeDuration;

    [Box("Hero Detection"), SerializeField] private float _minSpawnDistanceFromHero;
    [Box("Hero Detection"), SerializeField] private float _heroAheadOffset;
    [Box("Hero Detection"), SerializeField, LayerField] private int _heroLayer;

    public float MinSpawnDistanceFromHero => _minSpawnDistanceFromHero;
    
    // @formatter:on

    private CriminalState m_criminalState;

    private float m_directionChangeTimer;
    private float m_directionChangeDuration;
    private float m_sidewayDirection;

    public override void Init()
    {
        base.Init();

        Stop();
    }

    public override void Play()
    {
        base.Play();

        _rigidbody.angularVelocity = Vector3.zero;
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        SetCriminalState(CriminalState.Moving);
    }

    public override void Stop()
    {
        base.Stop();

        _rigidbody.angularVelocity = Vector3.zero;
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        SetCriminalState(CriminalState.Idle);

        m_directionChangeTimer = 0;
        m_directionChangeDuration = 0;
    }

    protected void SetCriminalState(CriminalState state)
    {
        if (m_criminalState == state) return;

        m_criminalState = state;
        switch (state)
        {
            case CriminalState.Idle:
                break;
            case CriminalState.Moving:
                m_directionChangeDuration = 0;
                break;
            case CriminalState.ChangingDirection:
                m_directionChangeDuration = Random.Range(_directionChangeDuration.x, _directionChangeDuration.y);
                m_sidewayDirection = Random.Range(-1, 1f);
                break;
        }
    }

    public void FixedUpdateCriminal()
    {
        if (!m_init || !m_playing) return;

        if (m_criminalState == CriminalState.Idle) return;

        var deltaTime = Time.deltaTime;
        m_directionChangeTimer += deltaTime;

        ApplyRotation();
        
        switch (m_criminalState)
        {
            case CriminalState.Moving:
                if (m_directionChangeTimer >= _directionChangeCooldown)
                {
                    m_directionChangeTimer = 0;
                    if (_directionChangeRate >= Random.value)
                    {
                        SetCriminalState(CriminalState.ChangingDirection);

                        goto case CriminalState.ChangingDirection;
                    }
                }

                ApplyMovement(GetForwardsMovement(IsHeroAhead()));
                break;
            case CriminalState.ChangingDirection:
                m_directionChangeDuration -= deltaTime;
                if (m_directionChangeDuration <= 0)
                {
                    SetCriminalState(CriminalState.Moving);
                    goto case CriminalState.Moving;
                }

                ApplyMovement(GetForwardsMovement(IsHeroAhead()) + GetSidewayMovement(m_sidewayDirection));
                break;
        }
    }

    private bool IsHeroAhead() => GameManager.Instance.HeroBehaviour.GetPosition().z >= GetPosition().z - _heroAheadOffset;

    protected abstract void OnHeroTriggered();
    public abstract void Hit();

    protected void Smash()
    {
        if (!m_init || !m_playing) return;

        Stop();
        _rigidbody.constraints = RigidbodyConstraints.None;

        if (GameManager.Instance.LevelManager.SpawnNextCriminal(GameManager.Instance.HeroBehaviour.GetPosition()))
            GameManager.Instance.LevelManager.PlayCurrentCriminal();
    }

    private void OnTriggerStay(Collider other)
    {
        if (m_criminalState == CriminalState.Idle) return;

        if (other.gameObject.layer == _heroLayer)
            OnHeroTriggered();
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(GetPosition() - Vector3.forward * _minSpawnDistanceFromHero + Vector3.up * .45f, .25f);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(GetPosition() - Vector3.forward * _heroAheadOffset + Vector3.up * .45f, .25f);
    }
#endif
}
﻿using EncryptionTool;
using MightyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private enum GameState : byte
    {
        Init,
        WaitForPlay,
        Playing,
        EndLevel,
    }
    
    public static GameManager Instance { get; private set; }

    [SerializeField, FindObject] private EncryptionInitializer _encryptionInitializer;
    
    [SerializeField, FindAssets, Reorderable(false, options: ArrayOption.DisableSizeField)]
    private LevelModel[] _levels;

    [SerializeField, FindObject] private GUIManager _guiManager;

    [SerializeField, FindObject] private HeroBehaviour _heroBehaviour;

    private GameState m_gameState;

    public GUIManager GUIManager => _guiManager;
    public LevelManager LevelManager { get; private set; }

    public HeroBehaviour HeroBehaviour => _heroBehaviour;
    
    private void Start() => Init();

    public void Init()
    {
        Instance = this;

        m_gameState = GameState.Init;

        SavedDataServices.Init(_encryptionInitializer);
        SavedDataServices.LoadEverythingFromLocal();
        
        LoadLevel(SavedDataServices.LevelIndex);
    }

    public void Play()
    {
        m_gameState = GameState.Playing;
        
        _guiManager.DisplayStartPanel(false);
        _guiManager.DisplayNextButton(false);
        _guiManager.DisplayRestartButton(false);
        
        _heroBehaviour.Play();
        LevelManager.PlayLevel();
    }
    
    private void Update()
    {
        switch (m_gameState)
        {
            case GameState.WaitForPlay:
                if (Input.GetMouseButtonDown(0))
                    Play();
                break;
            case GameState.Playing:
                _heroBehaviour.UpdateHero();
                break;
        }
    }

    private void FixedUpdate()
    {
        if (m_gameState != GameState.Playing) return;
        
        _heroBehaviour.FixedUpdateHero();
        LevelManager.FixedUpdateManager();
    }

    public void WaitForPlay(LevelManager levelManager)
    {
        m_gameState = GameState.WaitForPlay;
        
        LevelManager = levelManager;
    }

    public void WaitForRestart()
    {
        m_gameState = GameState.EndLevel;
        
        _heroBehaviour.Stop();

        _guiManager.DisplayStartPanel(false);
        _guiManager.DisplayNextButton(false);
        _guiManager.DisplayRestartButton(true);
    }

    public void WaitForNext()
    {
        m_gameState = GameState.EndLevel;
        
        _heroBehaviour.Stop();
        
        _guiManager.DisplayStartPanel(false);
        _guiManager.DisplayNextButton(true);
        _guiManager.DisplayRestartButton(false);
    }

    public void LoadNextLevel()
    {
        var index = SavedDataServices.LevelIndex;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        LoadLevel(++index);
    }

    public void LoadLevel(byte index, bool saveProgression = true)
    {
        _guiManager.Init();
     
        if (index >= _levels.Length) index = 0;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        if (saveProgression) SavedDataServices.LevelIndex = index;

        SceneManager.LoadScene(_levels[index].SceneIndex, LoadSceneMode.Additive);
    }

    public void UnloadLevel(byte index) => SceneManager.UnloadSceneAsync(_levels[index].SceneIndex);

    public bool IsLevelLoaded(byte index) => _levels[index].IsLoaded();

    public void ReloadLevel() => LoadLevel(SavedDataServices.LevelIndex, false);
}
﻿using MightyAttributes;
using MightyAttributes.Utilities;
using UnityEngine;

public class TrackFragmentBehaviour : MonoBehaviour
{
    [SerializeField] private LayerMask _movingMask;

    [SerializeField] private bool _firstOfPattern;
    [SerializeField, ShowIf("_firstOfPattern")] private TrackPatternBehaviour _trackPattern;

    private void OnTriggerEnter(Collider other)
    {
        var go = other.gameObject;
        if (!_movingMask.Contains(go.layer)) return;
        
        var moving = go.GetComponent<BaseMovingBehaviour>();
        moving.LookAt(transform.forward);
        
        if (!_firstOfPattern || !(moving is HeroBehaviour)) return;
        
        GameManager.Instance.LevelManager.LoadNextPattern(_trackPattern);
        GameManager.Instance.LevelManager.PlayPattern(_trackPattern);
    }
}
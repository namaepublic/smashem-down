﻿using System.Linq;
using MightyAttributes.Utilities;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class SlotBehaviour : MonoBehaviour
{
    [SerializeField] private bool _leftLane;

    public BaseCriminalBehaviour SpawnCriminal(BaseCriminalBehaviour criminalPrefab)
    {
        var tfm = transform;
        return Instantiate(criminalPrefab, tfm.position, tfm.rotation, GameManager.Instance.LevelManager.CriminalsTransform);
    }

    public VehicleBehaviour SpawnVehicle(VehicleBehaviour vehiclePrefab)
    {
        var tfm = transform;
        var vehicle = Instantiate(vehiclePrefab, tfm.position, tfm.rotation, tfm);
        
        vehicle.LeftLane = _leftLane;

        return vehicle;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!Selection.gameObjects.Any(x =>
            x.GetComponent<SlotBehaviour>() || x.GetComponent<TrackFragmentBehaviour>() || x.GetComponent<TrackPatternBehaviour>())) return;

        var tfm = transform;
        GizmosUtilities.DrawTopDownRectangle(tfm.position, tfm.rotation, new Vector2(.8f, 2.5f),
            new Color(1f, 0.92f, 0.02f, 0.16f), Color.yellow);
    }
#endif
}
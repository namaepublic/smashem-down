﻿using System.Collections.Generic;
using MightyAttributes;
using UnityEngine;

public class TrackBlockBehaviour : MonoBehaviour
{
    [SerializeField, GetComponentsInChildren, ReadOnly]
    private SlotBehaviour[] _slots;

    private List<SlotBehaviour> m_vehicleSlotBehaviours;

    public BaseCriminalBehaviour SpawnCriminal(BaseCriminalBehaviour criminalPrefab) =>
        m_vehicleSlotBehaviours?[Random.Range(0, m_vehicleSlotBehaviours.Count)].SpawnCriminal(criminalPrefab);

    public IEnumerable<VehicleBehaviour> SpawnVehicles(int amount, VehicleBehaviour[] prefabs)
    {
        m_vehicleSlotBehaviours = new List<SlotBehaviour>(_slots);

        var maxAmount = Mathf.Min(amount, _slots.Length);
        
        for (var i = 0; i < maxAmount; i++)
        {
            var index = Random.Range(0, m_vehicleSlotBehaviours.Count);
            yield return m_vehicleSlotBehaviours[index].SpawnVehicle(prefabs[Random.Range(0, prefabs.Length)]);
            m_vehicleSlotBehaviours.RemoveAt(index);
        }
    }

#if UNITY_EDITOR
    public int MaxAmountOfVehicles => _slots.Length;
#endif
}
﻿using System.Collections.Generic;
using MightyAttributes;
using UnityEngine;

public class TrackPatternBehaviour : MonoBehaviour
{
    // @formatter:off
    
    [SerializeField, PositionHandle(true)] private Vector3 _endPosition;
    [SerializeField] private Vector3 _endRotation;

    [SerializeField, GetComponentsInChildren, ReadOnly] private TrackBlockBehaviour[] _trackBlocks;
    
    // @formatter:on

    public TrackBlockBehaviour[] TrackBlocks => _trackBlocks;
    
    public List<VehicleBehaviour> VehicleBehaviours { get; } = new List<VehicleBehaviour>();

    public Vector3 GetEndPosition() => transform.position + _endPosition;
    public Vector3 EndRotation => _endRotation;

    public BaseCriminalBehaviour SpawnCriminal(Vector3 heroPosition, BaseCriminalBehaviour criminalPrefab)
    {
        foreach (var track in _trackBlocks)
        {
            if (track.transform.position.z - heroPosition.z < criminalPrefab.MinSpawnDistanceFromHero) continue;

            return track.SpawnCriminal(criminalPrefab);
        }

        return null;
    }

    public void SpawnVehicles(TrackSection section)
    {
        VehicleBehaviours.Clear();

        for (var i = 0; i < _trackBlocks.Length; i++)
            VehicleBehaviours.AddRange(_trackBlocks[i].SpawnVehicles(section.GetAmountOfVehicles(), section.VehiclePrefabs));
    }
    
#if UNITY_EDITOR
    public int GetMaxAmountOfVehicles()
    {
        var maxAmount = 0;
        foreach (var trackBlock in _trackBlocks)
        {
            if (trackBlock.MaxAmountOfVehicles > maxAmount)
                maxAmount = trackBlock.MaxAmountOfVehicles;
        }

        return maxAmount;
    }
#endif
}
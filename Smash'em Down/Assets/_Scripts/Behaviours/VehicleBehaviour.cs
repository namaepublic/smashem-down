﻿using MightyAttributes.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

public class VehicleBehaviour : BaseMovingBehaviour
{
    protected enum VehicleState : byte
    {
        Idle,
        Moving,
        Smashed
    }

    [SerializeField] private LayerMask _smasherMask;
    [SerializeField] private float _explosionForce;

    private VehicleState m_vehicleState;
    public bool LeftLane { get; set; }

    public override void Init()
    {
        base.Init();

        if (LeftLane)
            m_transform.Rotate(Vector3.up, 180);

        Stop();
    }

    public override void Play()
    {
        if (m_vehicleState == VehicleState.Smashed) return;

        base.Play();

        _rigidbody.angularVelocity = Vector3.zero;
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        SetVehicleState(VehicleState.Moving);
    }

    public override void Stop()
    {
        base.Stop();

        _rigidbody.angularVelocity = Vector3.zero;
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        SetVehicleState(VehicleState.Idle);
    }

    private void SetVehicleState(VehicleState state)
    {
        if (m_vehicleState == state) return;

        m_vehicleState = state;
        switch (state)
        {
            case VehicleState.Idle:
                break;
            case VehicleState.Moving:
                break;
            case VehicleState.Smashed:
                break;
        }
    }

    public void FixedUpdateVehicle()
    {
        if (!m_init || !m_playing) return;

        if (m_vehicleState == VehicleState.Idle) return;

        ApplyRotation();
        ApplyMovement(GetForwardsMovement());
    }

    private Vector3 GetForwardsMovement() => _moveSpeed * Time.deltaTime * m_transform.forward;

    public override void LookAt(Vector3 direction)
    {
        if (!m_init || !m_playing) return;
        
        m_changingRotation = true;
        m_forward = LeftLane ? -direction : direction;
    }

    private void Smash()
    {
        if (!m_init) return;

        Stop();

        _rigidbody.constraints = RigidbodyConstraints.None;

        _rigidbody.AddForce(_explosionForce * new Vector3(Random.Range(-1, 1f), Random.value, Random.value), ForceMode.Impulse);
        _rigidbody.AddTorque(_explosionForce * Random.insideUnitSphere, ForceMode.Impulse);

        SetVehicleState(VehicleState.Smashed);
    }

    private void OnTriggerEnter(Collider other)
    {
        var go = other.gameObject;
        if (!_smasherMask.Contains(go.layer)) return;

        var character = go.GetComponent<BaseCharacterBehaviour>();

        switch (character)
        {
            case BaseCriminalBehaviour _:
                Smash();
                break;
            case HeroBehaviour hero:
                if (!hero.IsShielding)
                    hero.LoseLife();

                Smash();
                break;
        }
    }
}
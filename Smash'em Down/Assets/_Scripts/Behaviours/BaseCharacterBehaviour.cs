﻿using MightyAttributes;
using UnityEngine;

public abstract class BaseCharacterBehaviour : BaseMovingBehaviour
{
    // @formatter:off

    [Box("Movements"), SerializeField] private float _sprintSpeed;
    [Box("Movements"), SerializeField] private float _sidewaySpeed;
    [Box("Movements"), SerializeField] private float _maxSidewaySpeed;
    
    // @formatter:on

    protected Vector3 GetForwardsMovement(bool sprinting) => Time.deltaTime * (sprinting ? _sprintSpeed : _moveSpeed) * m_transform.forward;

    protected Vector3 GetSidewayMovement(float mouseDelta) =>
        Time.deltaTime * Mathf.Clamp(mouseDelta * _sidewaySpeed, -_maxSidewaySpeed, _maxSidewaySpeed) * m_transform.right;
}
﻿using MightyAttributes;
using UnityEngine;

public abstract class BaseMovingBehaviour : MonoBehaviour
{
    // @formatter:off
    
    [Box("Movements"), SerializeField, GetComponent, ReadOnly] protected Rigidbody _rigidbody;
    [Box("Movements"), SerializeField] protected float _moveSpeed;
    [Box("Movements"), SerializeField] protected float _lookAtSpeed;
    
    // @formatter:on
    
    protected bool m_init;
    protected bool m_playing;
    protected Transform m_transform;

    protected bool m_changingRotation;
    protected Vector3 m_forward;
    
    public virtual void Init()
    {
        if (!m_init) m_transform = transform;

        m_init = true;
    }

    public virtual void Play() => m_playing = true;

    public virtual void Stop() => m_playing = false;

    public Vector3 GetPosition() => _rigidbody.position;

    public virtual void LookAt(Vector3 direction)
    {
        if (!m_init || !m_playing) return;
        
        m_changingRotation = true;
        m_forward = direction;
    }

    protected void ApplyMovement(Vector3 movement)
    {
        if (!m_init || !m_playing) return;

        _rigidbody.MovePosition(GetPosition() + movement);
    }
    
    protected void ApplyRotation()
    {
        if (!m_changingRotation) return;
        
        m_transform.forward = Vector3.MoveTowards(m_transform.forward, m_forward, _lookAtSpeed * Time.deltaTime);

        if (!((m_transform.forward - m_forward).sqrMagnitude <= 0.001)) return;
            
        m_transform.forward = m_forward;
        m_changingRotation = false;
    }
}